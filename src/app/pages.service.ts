import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from "@angular/common/http"
import { API_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  pages: any = []

  updated = new Subject();

  constructor(private http: HttpClient) { }

  updatePages(){
    console.log("requesting page status")
    this.http.get(API_URL + "/status").subscribe(data => {
      this.pages = data
      this.updated.next();
    })
  }
}
