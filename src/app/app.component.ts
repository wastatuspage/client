import { Component, ViewContainerRef, AfterViewInit, ViewChild } from '@angular/core';
import { PagesService } from './pages.service';

@Component({
  selector: 'sp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements AfterViewInit{
  title = 'StatusPage';

  pages: any[] = []

  focused: number = 0

  @ViewChild("timelineRef", {static: false}) timelineRef;

  constructor(private pagesService: PagesService){
    this.pagesService.updated.subscribe(() => {
      this.pages = this.pagesService.pages;
    })

    this.pagesService.updatePages();
  }

  ngAfterViewInit(){
    console.log(this.timelineRef)
  }

  get statusReport(): string{
    var offlinePages = this.offlinePages;
    if(offlinePages.length == 0){
      return "All services are online";
    }else{
      return "Some services are offline!";
    }
  }

  get statusReportOnline(): boolean{
    return this.offlinePages.length == 0;
  }

  get offlinePages(){
    var offlinePages = [];
    for(let p of this.pages){
      if(!p.status.online){
        offlinePages.push(offlinePages);
      }
    }
    return offlinePages;
  }

  get focusedPage(){
    for(let p of this.pages){
      if(p.id == this.focused){
        return p;
      }
    }
  }

  get timeline(): any[]{
    var timeline = [];
    var current = new Date().getTime();

    for(let i = 50; i >= 0; i--){
      var timeStart = current - ((i + 1) * this.focusedPage.interval)
      var timeEnd = current -(i * this.focusedPage.interval);
      for(let incident of this.focusedPage.status.incidents){
        
      }
      timeline.push({fill: "#00ff00"})
    }

    return timeline;
  }

  // get timelinePoints(): number{
    
  // }

  getIncidentEventName(event){
    switch(event){
      case 0: return "Downtime"
    }
  }

  getIncidentTimeSpan(start, end){
    return Math.floor((end - start) / 1000 / 60) + "min"
  }
}
